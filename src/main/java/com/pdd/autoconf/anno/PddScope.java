package com.pdd.autoconf.anno;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author:liyangpeng
 * @date:2020/8/28 16:27
 */
@Target({ElementType.TYPE,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Scope(value=com.pdd.autoconf.scope.PddScope.scopeName,proxyMode=ScopedProxyMode.TARGET_CLASS)
public @interface PddScope {

}
