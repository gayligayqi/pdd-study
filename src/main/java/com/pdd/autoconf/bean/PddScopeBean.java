package com.pdd.autoconf.bean;

import com.pdd.autoconf.anno.PddScope;
import lombok.Data;
import lombok.ToString;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * @author:liyangpeng
 * @date:2020/8/28 16:31
 */
@Component
@Data
@ToString
@PddScope
public class PddScopeBean {

    private String name;

    public PddScopeBean() {
        this.name= UUID.randomUUID().toString();
    }
}
