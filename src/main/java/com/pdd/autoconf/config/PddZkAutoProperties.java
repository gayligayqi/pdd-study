package com.pdd.autoconf.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author:liyangpeng
 * @date:2020/9/1 11:44
 */
@Data
@Component
@ConfigurationProperties(prefix = "pdd.config")
public class PddZkAutoProperties {

    private String basepath;

    private String nodes;

    private Integer baseSleepTimeMs;

    private Integer maxRetries;

    private Integer sessionTimeoutMs;

    private Integer connectionTimeoutMs;
}
