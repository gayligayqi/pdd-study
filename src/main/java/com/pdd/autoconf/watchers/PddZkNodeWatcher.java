package com.pdd.autoconf.watchers;

import com.pdd.autoconf.scope.PddScope;
import lombok.extern.slf4j.Slf4j;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.cache.TreeCacheEvent;
import org.apache.curator.framework.recipes.cache.TreeCacheListener;

/**
 * @author:liyangpeng
 * @date:2020/9/1 14:04
 */
@Slf4j
public class PddZkNodeWatcher implements TreeCacheListener {

    private PddScope pddScope;

    public PddZkNodeWatcher(PddScope pddScope){
        this.pddScope=pddScope;
    }
    @Override
    public void childEvent(CuratorFramework curatorFramework, TreeCacheEvent treeCacheEvent) throws Exception {
        if(treeCacheEvent.getType()==TreeCacheEvent.Type.NODE_UPDATED||treeCacheEvent.getType()==TreeCacheEvent.Type.NODE_REMOVED){
            log.info("refush node data:{}",treeCacheEvent.getData());
            pddScope.refreshAll();
        }
    }
}
