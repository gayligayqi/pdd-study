package com.pdd.autoconf.web;

import com.pdd.autoconf.bean.PddScopeBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author:liyangpeng
 * @date:2020/8/28 17:23
 */
@RestController
@RequestMapping("/autoconfig")
public class AutoConfigController {

    @Autowired
    private PddScopeBean pddScopeBean;

    @GetMapping("/get")
    public String get(){
        return pddScopeBean.getName();
    }
}
