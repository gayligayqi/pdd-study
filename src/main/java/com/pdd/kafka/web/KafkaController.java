package com.pdd.kafka.web;

import com.pdd.kafka.config.KafkaProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author:liyangpeng
 * @date:2020/4/21 15:36
 */
@RestController
@RequestMapping("/kafka")
public class KafkaController {

    @Autowired
    private KafkaProducer kafkaProducer;

    /**
     * kafka消息发送
     * @param message
     * @return
     */
    @GetMapping("/send")
    public String send(String message){
        kafkaProducer.send(message);
        return "success";
    }
}
