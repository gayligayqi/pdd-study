package com.pdd.rabbitmq.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * @author:liyangpeng
 * rabbitmq延时队列
 * @date:2020/4/14 17:35
 */
@Configuration
public class MessageQueueConfig {

    public static final String ORDER_QUEUE_NAME="delay_orderQueue";

    public static final String QUEUE_ROUTING_KEY="delay_order_routing_key";

    public static final String ORDER_EXCHANGE_NAME="delay_order_exchange";

    @Bean
    public Queue orderQueue(){
        return new Queue(ORDER_QUEUE_NAME,true);
    }

    @Bean
    public CustomExchange orderExchange(){
        Map<String, Object> args = new HashMap<>();
        args.put("x-delayed-type", "direct");
        return new CustomExchange(ORDER_EXCHANGE_NAME, "x-delayed-message", true, false, args);
    }

    @Bean
    public Binding bindExChange(@Qualifier("orderQueue")Queue queue, @Qualifier("orderExchange")CustomExchange customExchange){
        return BindingBuilder.bind(queue).to(customExchange).with(QUEUE_ROUTING_KEY).noargs();
    }
}
