//package com.pdd.rabbitmq.web;
//
//import com.pdd.rabbitmq.config.SenderService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.util.StringUtils;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.util.UUID;
//
///**
// * @author:liyangpeng
// * @date:2020/4/14 17:50
// */
//@RestController
//@RequestMapping("/test")
//public class MqController {
//
//    @Autowired
//    private SenderService senderService;
//
//    @GetMapping("/send")
//    public String test(String context){
//        if(StringUtils.isEmpty(context)){
//            return "context is not empty!";
//        }
//        senderService.sender(context, UUID.randomUUID().toString());
//        return "success";
//    }
//}
