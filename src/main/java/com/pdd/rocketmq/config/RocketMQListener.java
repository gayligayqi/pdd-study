package com.pdd.rocketmq.config;

import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.springframework.stereotype.Component;

/**
 * @author:liyangpeng
 * @date:2020/8/24 16:29
 */
@Component
@RocketMQMessageListener(topic = "pdd-topic-1",consumerGroup = "pdd-rocketmq",selectorExpression = "TagA")
public class RocketMQListener implements org.apache.rocketmq.spring.core.RocketMQListener<String> {

    @Override
    public void onMessage(String s) {
        System.out.println(s);
    }
}
