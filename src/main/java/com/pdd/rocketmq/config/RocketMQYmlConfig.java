//package com.pdd.rocketmq.config;
//
//import lombok.Data;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.context.annotation.Configuration;
//
///**
// * @author:liyangpeng
// * @date:2020/8/24 11:24
// */
//@Data
//@Configuration
//@ConfigurationProperties(prefix = "spring.rocketmq")
//public class RocketMQYmlConfig {
//
//    private String node;
//
//    private String groupName;
//}
